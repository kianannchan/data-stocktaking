# Data stocktaking
*Data stocktaking is a tool developed in python 3.8.0. Objective of the tool allow users to generate a report base on the age of the files in a specific folder and and subfolders, which uncover those uncessary files that resides without knowing it.From buisness perspective, it may be utilise as an audit tool to identify the unknowns. However do note that this tool is experimental, hence is still under development stage.*


#
### Features
- [x] Selection of base folder path
- [x] Define file age threshhold
- [x] Selection of file extenstion to scan (default is txt,xls,xlsx,json)
- [x] Generate report in excel format
- [x] Report authenticity validiation user interface and command line support



#
### Software Model

```mermaid
graph TD;
  app.py-->core.py!View;
  core.py!View-->core.py!controller
```

#
### Demonstration
[![Data Stocktaking App](https://s4.gifyu.com/images/data-stocktaking-app.gif)](https://gifyu.com/image/Yppp)



#
### Usage
```
pip install -r requirement.txt
python app.py
```

#
### Development
- [ ] Support in other platform


