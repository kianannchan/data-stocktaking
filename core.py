import os
import time
import xlsxwriter
import PySimpleGUI as sg
import hashlib
import openpyxl



class controller:
    def __init__(self):
        self.ext = ['txt', 'xls', 'xlsx', 'json']
        self.exit_code = sg.WIN_CLOSED

    def update_list(self, new_value):
        self.ext.append(new_value)

    def remove_list(self, value):
        self.ext.remove(value)

    def getlocaltime(self, format="%d/%m/%Y %H:%M:%S"):
        t = time.localtime()
        return time.strftime(format, t)    

    def openReport(self, report_path):
        os.startfile(report_path)

    def create_checksum(self, raw_value):
        return hashlib.md5(raw_value.encode()).hexdigest() 

    def check_checksum(self, file_name):
        md5 = ''
        wb = openpyxl.load_workbook(file_name)
        sheet = wb.active
        max_row = sheet.max_row
        max_col = sheet.max_column
        for row in range (6, max_row + 1):
            for col in range(1, max_col + 1):
                if col == 2 or col == 1:
                    md5 = md5 + (sheet.cell(row= row, column =col).hyperlink.target).replace('file:///','').replace('%20', ' ')
                else:
                    md5 = md5 + str(sheet.cell(row= row, column =col).value)

        if self.create_checksum(md5) == sheet.cell(row= 4, column =2).value: 
            return True 
        else:
            return False
        


    def generate_report(self, file_path, threshhold):
        md5 = ''
        report_file = f'{self.getlocaltime("%d%m%Y%H%M%S")}.xlsx'
        workbook = xlsxwriter.Workbook(report_file)
        worksheet = workbook.add_worksheet()
        worksheet.freeze_panes(5, 0)
        worksheet.set_column('A:A', 50)
        worksheet.set_column('B:B', 100)

        # define header format
        header_format = workbook.add_format({'bold': True,
                                     'align': 'center',
                                     'valign': 'vcenter',
                                     'fg_color': '#D7E4BC',
                                     'border': 1})

        # create header
        worksheet.write('A5', 'File', header_format)
        worksheet.write('B5', 'Path', header_format)
        worksheet.write('C5', 'Ext', header_format)
        worksheet.write('D5', 'Age', header_format)

        # starting row 
        row_index = 6

        # recursive search between root folders and sub folders
        for path, subdirs, files in os.walk(file_path):
            for name in files:
                file_name_path = os.path.join(path, name)
                file_name, file_extension = os.path.splitext(name)
                file_extension = file_extension.replace('.', '')

                if file_extension in self.ext:
                    time_diff = int((time.time() - os.path.getmtime(file_name_path)) /86400)
                    worksheet.write_url('A' + str(row_index), file_name_path, string=file_name)
                    worksheet.write_url('B' + str(row_index), path, string=path)

                    worksheet.write('C' + str(row_index), file_extension)
                    if time_diff > int(threshhold):
                        cell_format = workbook.add_format({'font_color': 'red'})
                        worksheet.write('D' + str(row_index), time_diff, cell_format)
                    else:
                        worksheet.write('D' + str(row_index), time_diff)

                    row_index = row_index + 1
                    md5 = md5 + file_name_path + path + file_extension + str(time_diff)

    
        # report detail
        worksheet.write('A1', 'Date:')
        worksheet.write('B1', self.getlocaltime())
        worksheet.write('A2', 'Base Path:')
        worksheet.write('B2', file_path)
        worksheet.write('A3', 'Total File(s):')
        worksheet.write('B3', str(row_index - 6))
        worksheet.write('A4', 'File Integrity:')
        worksheet.write('B4', self.create_checksum(md5))

        # save file
        workbook.close()
        self.openReport(report_file)



class view(controller):
    def __init__(self):
        controller.__init__(self)  

        sg.theme('DarkGrey14') 
        layout_1 = [  [sg.Text('Scan Folder')],
            [sg.In(key='input', size=(40, 1)), sg.FolderBrowse(target='input')], 
            [sg.Text('Age Threshold')],
            [sg.Spin(key= "age", values=[i for i in range(1, 9999)], initial_value=20, size=(47, 1))],
            [sg.Text('Extension Scanning')],
            [sg.Listbox(values=self.ext, size=(47, 12), key='-LIST-', enable_events=True)],
            [sg.Button('Add', size=(20, 1)), sg.Button('Remove', size=(20, 1))],
            [sg.HorizontalSeparator(pad=(6,20))],
            [sg.OK(), sg.Cancel()]] 

        layout_2 = [
             [sg.Text('Report Selection')],
             [sg.In(key='input2', size=(40, 1)), sg.FileBrowse(target='input2', key='validate_input')],
             [sg.Text('Status: ')],
             [sg.Multiline(size=(47, 5),disabled=True, key='validate_value')],
             [sg.Button('Verify', size=(43, 1))] 
             ]

        layout = [[sg.TabGroup([[sg.Tab('Generating Report', layout_1), sg.Tab('Validating Report', layout_2)]])]]

        self.window = sg.Window('Data Stocktaking', layout)

    
    def update_value(self,element, new_value):
        self.window[element].update(new_value)

    def pop_up_get_text(self,title, question):
        return sg.popup_get_text(title, question)

    def pop_up_alert(self,remarks):
        sg.popup(remarks)

    def close_app(self):
        self.window.close()


