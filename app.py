from core import view
import sys

# ini view obj
view = view()

# get argument
get_arg = sys.argv

# determine if its a command line session
if len(get_arg) > 1:
    # check file integrity, return True if pass, False if fail
    validate = view.check_checksum(get_arg[1])
    if validate:
        msg = 'Pass'
    else:
        msg = 'Fail'
    # output msg
    view.pop_up_alert(msg)
    # end program
    sys.exit()


while True:  # Event Loop
    event, values = view.window.read()

    if event in (view.exit_code, 'Exit'):
        break
    if event == 'Add':
        new_value = view.pop_up_get_text('Extension Addition', 'Please input a new extension below:')
        if new_value is not None and new_value not in view.ext:
            if len(new_value) > 0:
                view.update_list(new_value)
                view.update_value('-LIST-', view.ext)

    elif event == "Remove" and values['-LIST-'] is not None:
        if len(values['-LIST-']):
            view.remove_list(values['-LIST-'][0])
            view.update_value('-LIST-', view.ext)
        

    elif values['Browse'] is not None and event == "OK":
        if len(values['Browse']) > 0:
            view.generate_report (values['Browse'], values['age'])
            view.pop_up_alert('Report Generated')

    elif event == "Verify":
        if len(values['validate_input']) > 0:
            validate = view.check_checksum(values['validate_input'])
            if (validate):
                msg = 'Pass'
            else:
                msg = 'Fail'
            view.update_value('validate_value', msg)
            
            
        

view.close_app()

